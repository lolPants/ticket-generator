import io
import os
import asyncio
from uuid import uuid4
from urllib.parse import unquote
from ticket import generate_ticket
from sanic import Sanic
from sanic.exceptions import abort
from sanic.response import file, text

app = Sanic()

async def remove_file(file_name):
  await asyncio.sleep(2)
  os.remove(file_name)

@app.route('/')
async def index(request):
  return await file('index.txt')

@app.route('/api/v1.0/lords/<input_str>')
async def lords(request, input_str):
  label = unquote(input_str)
  if len(label) > 30:
    return abort(400)
  
  file_name = '{}.png'.format(uuid4())

  image = generate_ticket(label, 'The House of Lords')
  image.save(file_name, 'PNG')
  loop = asyncio.get_event_loop()
  loop.create_task(remove_file(file_name))

  return await file(file_name)

@app.route('/api/v1.0/ts/<input_str>')
async def ts(request, input_str):
  label = unquote(input_str)
  if len(label) > 30:
    return abort(400)
  
  file_name = '{}.png'.format(uuid4())

  image = generate_ticket(label, 'The House of Lords')
  image.thumbnail((640, 640))
  image.save(file_name, 'PNG')
  loop = asyncio.get_event_loop()
  loop.create_task(remove_file(file_name))

  return await file(file_name)

@app.route('/api/v1.0/numberwang/<input_str>')
async def numberwang(request, input_str):
  label = unquote(input_str)
  if len(label) > 30:
    return abort(400)
  
  file_name = '{}.png'.format(uuid4())

  image = generate_ticket(label, 'Historic Numberwang')
  image.save(file_name, 'PNG')
  loop = asyncio.get_event_loop()
  loop.create_task(remove_file(file_name))

  return await file(file_name)

@app.route('/api/v1.0/modoffice/<input_str>')
async def modoffice(request, input_str):
  label = unquote(input_str)
  if len(label) > 30:
    return abort(400)
  
  file_name = '{}.png'.format(uuid4())

  image = generate_ticket(label, 'Mod Office')
  image.save(file_name, 'PNG')
  loop = asyncio.get_event_loop()
  loop.create_task(remove_file(file_name))

  return await file(file_name)

@app.route('/api/v1.0/techoffice/<input_str>')
async def techoffice(request, input_str):
  label = unquote(input_str)
  if len(label) > 30:
    return abort(400)
  
  file_name = '{}.png'.format(uuid4())

  image = generate_ticket(label, 'Tech Office')
  image.save(file_name, 'PNG')
  loop = asyncio.get_event_loop()
  loop.create_task(remove_file(file_name))

  return await file(file_name)

@app.route('/api/v1.0/afk/<input_str>')
async def afk(request, input_str):
  label = unquote(input_str)
  if len(label) > 30:
    return abort(400)
  
  file_name = '{}.png'.format(uuid4())

  image = generate_ticket(label, 'AFK')
  image.save(file_name, 'PNG')
  loop = asyncio.get_event_loop()
  loop.create_task(remove_file(file_name))

  return await file(file_name)

if __name__ == '__main__':
  app.run(host='0.0.0.0', port=8080)
