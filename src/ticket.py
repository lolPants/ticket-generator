import random
import datetime
from PIL import Image, ImageFont, ImageDraw

# Ticket Dimensions
box_size = (350, 44)
x_offset = 27
from_offset = (x_offset, 361)
to_offset = (x_offset, 453)

def get_font(text, target_size):
  size_check = 1
  while True:
    test_font = ImageFont.truetype(font='./hypermarket.ttf', size=size_check)
    font_size = test_font.getsize(text)
    if font_size[0] > target_size[0] or font_size[1] > target_size[1]:
      return font
    else:
      font = test_font
      size_check += 1

def calculate_size(target_size, actual_size, offset=(0, 0)):
  t_x = target_size[0]
  t_y = target_size[1]

  a_x = actual_size[0]
  a_y = actual_size[1]

  o_x = offset[0]
  o_y = offset[1]

  x = ((t_x / 2) - (a_x / 2)) + o_x
  y = ((t_y / 2) - (a_y / 2)) + o_y

  return (x, y)

def calculate_x_y(text, target_size, offset=(0, 0), **kwargs):
  if 'font' in kwargs:
    font = kwargs['font']
  else:
    font = get_font(text, target_size)

  size = font.getsize(text)
  return calculate_size(target_size, size, offset)

def draw_text(image, text, target_size, offset=(0, 0), fill=(45, 33, 19, 240)):
  font = get_font(text, target_size)
  pos = calculate_x_y(text, target_size, offset, font=font)

  d = ImageDraw.Draw(image)
  d.text((offset[0], pos[1]), text, font=font, fill=fill)
  del d

  return image

def generate_ticket(from_str, to_str):
  image = Image.open('ticket.png').convert('RGBA')
  
  # From and To
  draw_text(image, from_str.upper(), box_size, offset=from_offset)
  draw_text(image, to_str.upper(), box_size, offset=to_offset)

  now = datetime.datetime.now()
  months = ['nul', 'JNR', 'FBY', 'MCH', 'APR', 'MAY', 'JUN', 'JLY', 'AUG', 'SEP', 'OCT', 'NOV', 'DMR']
  date_str = '{}•{}•{}'.format(now.strftime('%d'), months[int(now.strftime('%m'))], now.strftime('%y'))
  time_str = now.strftime('%H:%M')
  time_short = now.strftime('%H%M')

  random.seed('{}-{}-{}'.format(from_str, to_str, time_short))
  ticket_num = str(random.randint(10000, 99999))

  # Start Date and Valid Until
  draw_text(image, date_str, (200, 45), offset=(290, 238))
  draw_text(image, date_str, (200, 45), offset=(419, 355))
  
  # Short Time
  draw_text(image, time_short, (90, 45), offset=(783, 455))
  
  # Ticket Number
  draw_text(image, ticket_num, (111, 50), offset=(545, 235))

  # Date and Time at the bottom
  draw_text(image, date_str, (125, 30), offset=(835, 588), fill=(80, 21, 0, 250))
  draw_text(image, time_str, (70, 30), offset=(725, 586), fill=(80, 21, 0, 250))

  return image
